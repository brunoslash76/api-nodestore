'use strict';
const mongoose = require('mongoose');
const Customer = mongoose.model('Customer');

exports.get = async () => {
    const res = await Customer
        .find({});
    return res;
}

exports.getBySlug = async (slug) => {
    const res = await Customer
        .findOne({
            active: true,
            slug: slug
        });
    return res;
}

exports.getById = async (id) => {
    const res = await Customer
        .findById(id);
    return res;
}

exports.getByTag = async (tag) => {
    const res = await Customer
        .find({
            tags: tag,
        });
    return res;
}

exports.create = async (data) => {
    var customer = new Customer(data);
    await customer.save();
}

exports.authenticate = async (data) => {
    const res = await Customer.findOne({
        email: data.email,
        password: data.password
    });
    return res;
}

exports.update = async (id, data) => {
    await Customer
        .findByIdAndUpdate(id, {
            $set: {
                title: data.title,
                description: data.description,
                price: data.price,
                slug: data.slug
            }
        });
}

exports.delete = async (id) => {
    await Customer
        .findOneAndRemove(id);
}